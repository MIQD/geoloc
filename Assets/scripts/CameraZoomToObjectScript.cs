﻿using UnityEngine;
using System.Collections;

public class CameraZoomToObjectScript : MonoBehaviour
{

    Camera cam;
    public Transform cameraDefaultTransform;

    public float zoomDistance = 0.6f;
    public float zoomSpeed = 0.05f;

    // Use this for initialization
    void Start()
    {

        cam = Camera.main;

    }

    public void ZoomAtObject(Vector3 startCamPos, Vector3 objectPos)
    {
        cameraDefaultTransform.localPosition = cam.transform.localPosition;
        cameraDefaultTransform.localRotation = cam.transform.rotation;

        StartCoroutine(ZoomInCoroutine(startCamPos, objectPos));
    }

    public void UnzoomFromObject()
    {
        StartCoroutine(UnzoomCoroutine());
    }

    IEnumerator ZoomInCoroutine(Vector3 startCamPos, Vector3 objectPos)
    {
        Vector3 direction;
        //Quaternion lookRotation;

        float t = 0;

        while (t < zoomDistance)
        {

            t += zoomSpeed;

            direction = (objectPos - startCamPos).normalized;
            //lookRotation = Quaternion.LookRotation(direction);

            //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, lookRotation, t);
            cam.transform.position = Vector3.Lerp(startCamPos, objectPos, t);
            yield return null;
        }
    }

    IEnumerator UnzoomCoroutine()
    {
        float cameraBack = 0;

        while (cameraBack < 1)
        {
            //cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, cameraDefaultTransform.rotation, cameraBack);

            cameraBack += zoomSpeed * 1.3f;
            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, cameraDefaultTransform.localPosition, cameraBack);
            yield return null;
        }

    }
}