﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    MarkerData[] mList;

    //public struct MarkerData
    //{
    //    public string name;
    //    public Vector2 localization;
    //    public int rank;
    //}

    public RectTransform markerDataParent;
    public Marker_ShowDataScript markerData; //prefab for showing data in panel
    Marker_ShowDataScript[] arrayOfMarkerData;//array of every collected marker

    public static GameController Instance;
    public List<Marker_ShowDataScript> collectedMarkers;//list of every collected marker
    public List<MarkerScript> markersReferencedToData;

    // Use this for initialization
    void Awake () {
        Instance = this;

        collectedMarkers = new List<Marker_ShowDataScript>();
        
    }
	
    public void ShowCollectedMarkers()
    {
        //then panel is opening, it instantiates a row with data for every collected marker
        if (!markerDataParent.parent.gameObject.activeSelf)
        {
            arrayOfMarkerData = new Marker_ShowDataScript[collectedMarkers.Count];
            mList = new MarkerData[collectedMarkers.Count];
            
            markerDataParent.parent.gameObject.SetActive(true);
            for (int i = 0; i < collectedMarkers.Count; i++)
            {
                Marker_ShowDataScript marker = Instantiate(markerData);
                arrayOfMarkerData[i] = marker;
                
                marker.transform.SetParent(markerDataParent);

                marker.transform.position = markerDataParent.transform.position;
                marker.transform.rotation = markerDataParent.transform.rotation;
                marker.transform.localScale = markerDataParent.transform.localScale;
                

                marker.number.text =  (i + 1).ToString();
                marker.name.text = collectedMarkers[i].markerData.myName;

                marker.coordinates.text = "Lat: " + collectedMarkers[i].markerData.localizationX + ",\n Long: " + collectedMarkers[i].markerData.localizationY;
                marker.rank.text = "" + collectedMarkers[i].markerData.rank;
                mList[i] = collectedMarkers[i].markerData;
            }
        }
        //then panel is closing, it destroys every row of data from collected markers
        //else
        //{
        //    ResetCollectedMarkerDatas(false);

        //    markerDataParent.parent.gameObject.SetActive(false);
        //}
    }

    //public void ResetCollectedMarkers()
    //{
    //    ResetCollectedMarkerDatas(true);
    //    markerDataParent.parent.gameObject.SetActive(false);
    //}

    //void ResetCollectedMarkerDatas(bool reserMarkerCollectBool)
    //{
    //    int lenght = arrayOfMarkerData.Length;
    //    for (int i = 0; i < arrayOfMarkerData.Length; i++)
    //    {
    //        Destroy(arrayOfMarkerData[i].gameObject);
    //        if (reserMarkerCollectBool)
    //        {
    //            markersReferencedToData[i].SetUnselected();
    //        }
    //    }
    //    if(reserMarkerCollectBool)
    //    {
    //        if (collectedMarkers.Count != 0)
    //        {
    //            collectedMarkers.RemoveRange(0, collectedMarkers.Count);
    //        }

    //        if (markersReferencedToData.Count != 0)
    //        {
    //            markersReferencedToData.RemoveRange(0, markersReferencedToData.Count);
    //        }
    //    }

    //    arrayOfMarkerData = new Marker_ShowDataScript[0];
    //    //Debug.Log(collectedMarkers.Count);
    //    //Debug.Log(markersReferencedToData.Count);

    //}

}
