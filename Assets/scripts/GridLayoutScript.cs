﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridLayoutScript : MonoBehaviour {

    public int cel, row;

    GridLayoutGroup grid;

    RectTransform parent;

    // Use this for initialization
    void Start () {
        
        grid = GetComponent<GridLayoutGroup>();

        parent = gameObject.GetComponent<RectTransform>();

        

	}
	
	// Update is called once per frame
	void Update () {
        grid.cellSize = new Vector2(parent.rect.width / cel, parent.rect.height / row);
    }
}
