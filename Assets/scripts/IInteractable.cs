﻿using UnityEngine;
using System.Collections;

public interface IInteractable {

    bool inRange { get; set;}

    void OnClickMethod();

    void ClickableApperance();
    void NotclickableApperance();
   
}
