﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class MarkerData
{
    public string myName;

    public float localizationX;
    public float localizationY;

    public int rank;
    public bool collected;

    public MarkerData()
    {
        myName = "default";
        localizationX = 0;
        localizationY = 0;
        rank = 1;
        collected = false;
    }
}
[Serializable]
public class MarkerDataCollection
{
    public MarkerData[] points;
    public MarkerDataCollection(int i)
    {
        points = new MarkerData[i];
    }
}
