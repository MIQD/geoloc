﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class MarkerManager : MonoBehaviour {

    public GameObject markerPrefab;
    MarkerData[] points;
    string json;
    string[] js;

    bool loaded;
    float height;

    MapNav mapNavScript;
    private float initX, initZ;

    public static MarkerDataCollection myList;
    public static Dictionary<string, MarkerData> markerDataDir;

    // Use this for initialization
    void Start ()
    {
        markerDataDir = new Dictionary<string, MarkerData>();

        mapNavScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<MapNav>();
        height = 3f;
    }

    void Update()
    {
        if (mapNavScript.ready && !loaded)
        {
            loaded = true;
            initX = mapNavScript.fixLon * 20037508.34f / 18000;
            initZ = (float)(System.Math.Log(System.Math.Tan((90 + mapNavScript.fixLat) * System.Math.PI / 360)) / (System.Math.PI / 180));
            initZ = initZ * 20037508.34f / 18000;
            TextAsset asset;
            string assetString;

           
              
                assetString = LoadTextAsset();
            
            if(assetString == null)
            {
                asset = Resources.Load("points") as TextAsset;
                assetString = asset.text;
                Debug.Log("stare");
            }

            //asset = Resources.Load("points") as TextAsset;

            Debug.Log(assetString);
            SetMarkers(assetString, initX, initZ, height);
        }
    }

    string LoadTextAsset()
    {
        if(File.Exists(Application.persistentDataPath + "/points.json"))
        {
            //BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(Application.persistentDataPath + "/points.dat", FileMode.Open);
            //FileStream file = File.Open(Application.persistentDataPath + "/points.json", FileMode.Open);
            //string Js = ;
            


            //string Js = (string)bf.Deserialize(file);
            //file.Close();
            //string Js = File.Open(Application.persistentDataPath + "/points.json", FileMode.Open).ToString();
            //file.Close();

            string Js = File.ReadAllText(Application.persistentDataPath + "/points.json");


            return Js;
        }
        else
        {
            return null;
        }
    }

    public void InstantiatePointsForTests()
    {
        points = new MarkerData[3];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = new MarkerData();
        }
        
        myList = new MarkerDataCollection(3);
        for (int j = 0; j < 3; j++)
        {
            points[j].myName = "marker" + j*1000;
            points[j].localizationX = 12.96671f + j * 4 / 4f;
            points[j].localizationY = 77.5972f + j * 4 / 4f;
            points[j].rank = 5 + j;
            points[j].collected = false;

            myList.points[j] = points[j];
        }
        json = JsonUtility.ToJson(myList);
        Debug.Log(json);
    }

    public void SetMarkers(string Json, float finitX, float finitZ, float fheight)
    {
        myList = JsonUtility.FromJson<MarkerDataCollection>(Json);

        foreach (MarkerData data in myList.points)
        {
            Vector3 position = new Vector3(((data.localizationY * 20037508.34f) / 18000) - finitX, fheight / 100, ((Mathf.Log(Mathf.Tan((90 + data.localizationX) * Mathf.PI / 360)) / (Mathf.PI / 180)) * 1113.19490777778f) - finitZ);
            Debug.Log(data.localizationX);
            GameObject newMarker = Instantiate(markerPrefab);
            newMarker.transform.SetParent(this.transform);
            newMarker.name = data.myName;
            newMarker.transform.position = position;
            
            MarkerScript newMarkerData = newMarker.GetComponent<MarkerScript>();
            newMarkerData.myName = data.myName;
            newMarkerData.localization = new Vector2(data.localizationX, data.localizationY);
            newMarkerData.rank = data.rank;
            newMarkerData.collected = data.collected;
            markerDataDir.Add(data.myName, data);

            if (!newMarkerData.collected)
            {
                newMarker.SetActive(true);
            }

            newMarkerData.SetupMarkerData();
        }
        SaveCollectedMarkers();
    }

    public static void SaveCollectedMarkers()
    {
        MarkerDataCollection collectionOfCollectedMarkers = new MarkerDataCollection(markerDataDir.Count);
       
        int i = 0;
        foreach(MarkerData data in markerDataDir.Values)
        {
            collectionOfCollectedMarkers.points[i] = new MarkerData();
            collectionOfCollectedMarkers.points[i].myName = data.myName;
            collectionOfCollectedMarkers.points[i].localizationX = data.localizationX;
            collectionOfCollectedMarkers.points[i].localizationY = data.localizationY;
            collectionOfCollectedMarkers.points[i].rank = data.rank;
            collectionOfCollectedMarkers.points[i].collected = data.collected;
            i++;
        }
        string Js = JsonUtility.ToJson(collectionOfCollectedMarkers, true);

        File.WriteAllText(Application.persistentDataPath + "/points.json", Js);
        
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Create(Application.persistentDataPath + "/points.dat");

        //bf.Serialize(file, Js);
        

        //File.WriteAllText(Application.dataPath + "/Resources/points.json", Js);
    }

    public void ResetMarkers()
    {
        MarkerDataCollection collectionOfCollectedMarkers = new MarkerDataCollection(markerDataDir.Count);
       
        int i = 0;
        foreach (MarkerData data in markerDataDir.Values)
        {
            collectionOfCollectedMarkers.points[i] = new MarkerData();
            collectionOfCollectedMarkers.points[i].myName = data.myName;
            collectionOfCollectedMarkers.points[i].localizationX = data.localizationX;
            collectionOfCollectedMarkers.points[i].localizationY = data.localizationY;
            collectionOfCollectedMarkers.points[i].rank = data.rank;
            collectionOfCollectedMarkers.points[i].collected = false;
            i++;
        }

        string Js = JsonUtility.ToJson(collectionOfCollectedMarkers, true);
        File.WriteAllText(Application.persistentDataPath + "/points.json", Js);

        // File.WriteAllText(Application.dataPath + "/Resources/points.json", Js);
    }
}
