﻿using UnityEngine;
using System.Collections;
using System;

public class MarkerScript : MonoBehaviour, IInteractable {
    
    public bool createdByJSon;

    public string myName;
    public Vector2 localization;
    public int rank;

    public GameObject canvas;
    public MarkerPanelScript markerPanel;
    public MarkerData mData;

    public bool collected;
    public float secondsToUncollect = 20;

    // GetDistance myGetDistance;

    MeshRenderer myMeshrenderer;

    public Color collectedColor;
    public Color userInRangeColor;
    Color defaultColor;

    Vector3 defaultScale;

    public bool inRange
    {
        get;
        set;
    }

    Marker_ShowDataScript myMarkerData;

    void Start()
    {
        defaultScale = transform.localScale;

        if(!createdByJSon)
        {
            SetupMarkerData();
        }
    }

    public void SetupMarkerData()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas");
        myMarkerData = new Marker_ShowDataScript();
        myMarkerData.markerData = new MarkerData();

        myMeshrenderer = GetComponent<MeshRenderer>();
        defaultColor = myMeshrenderer.material.color;

        //setting data for struct used by GameController for displaying markerData in collected panel;
        myMarkerData.markerData.myName = this.myName;
        myMarkerData.markerData.localizationX = this.localization.x;
        myMarkerData.markerData.localizationY = this.localization.y;
        myMarkerData.markerData.rank = this.rank;
        myMarkerData.markerData.collected = this.collected;

        if (collected)
        {
            GameController.Instance.collectedMarkers.Add(myMarkerData);
            GameController.Instance.markersReferencedToData.Add(this);
        }

        transform.position = new Vector3(transform.position.x, 1.2f, transform.position.z);
    }

    public void OnClickMethod()
    {
        if (inRange)
        {
            if (!collected)
            {
                Camera.main.GetComponent<CameraZoomToObjectScript>().ZoomAtObject(Camera.main.transform.position, transform.position);
                ActivateMarkerPanel();
            }
        }
    }

    //currently unused
    void FirstVisitingMethod()
    {
        myMeshrenderer.material.color = collectedColor;
    }

    void ActivateMarkerPanel()
    {
       MarkerPanelScript m = Instantiate(markerPanel);
       m.transform.parent = canvas.transform;
        m.GetComponent<RectTransform>().offsetMin = new Vector2(20, 20);
        m.GetComponent<RectTransform>().offsetMax = new Vector2(-20, -20);
        m.myName.text = myName;
        m.coordinates.text = localization.ToString();
        m.rank.text = rank.ToString();

        GetDistance.outOfRangeEvent += m.ActionsForClosingThatPanel;
        m.openedMarker = this;
        m.gameObject.SetActive(true);

        TouchInputController.freewalk = false;
    }

    public void CollectMarkerMethod()
    {
        collected = true;
        myMarkerData.markerData.collected = true;

        myMeshrenderer.material.color = collectedColor;

        GameController.Instance.collectedMarkers.Add(myMarkerData);
        GameController.Instance.markersReferencedToData.Add(this);

        gameObject.SetActive(false);

        MarkerManager.markerDataDir[myName].collected = true;
        MarkerManager.SaveCollectedMarkers();
    }

    public void ClickableApperance()
    {
        //transform.localScale = new Vector3(transform.localScale.x * 1.5f, transform.localScale.y * 2, transform.localScale.z * 1.5f);

        if (!collected)
        {
            if(myMeshrenderer == null)
            {
                myMeshrenderer = GetComponent<MeshRenderer>();
            }
            myMeshrenderer.material.color = userInRangeColor;
        }
    }

    public void NotclickableApperance()
    {
        //transform.localScale = defaultScale;

        if (!collected)
        {
            myMeshrenderer.material.color = defaultColor;
        }
    }

    //IEnumerator TimooutForSetingUncollected()
    //{
    //    yield return new WaitForSeconds(secondsToUncollect);
    //    collected = false;
    //    inRange = false;    //so if player is still in range it can detect marker as Clickable (Reference to this is in GetDistance.cs)
    //    NotclickableApperance();
    //    GameController.Instance.collectedMarkers.RemoveAt(0);
        
    //}

    //public void SetUnselected()
    //{
    //    collected = false;
    //    inRange = false;    //so if player is still in range it can detect marker as Clickable (Reference to this is in GetDistance.cs)
    //    NotclickableApperance();
    //}
}
