﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    Animator anim;
    MapNav map;

    Vector2 prevLocalization;
    Vector2 currentLocalization;

    bool flag = false;

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        map = FindObjectOfType<MapNav>();

        currentLocalization = new Vector2(map.userLat, map.userLon);
        StartCoroutine(AnimationController());
	}

    IEnumerator AnimationController()
    {
        while (true)
        {
            prevLocalization = currentLocalization;
            currentLocalization = new Vector2(map.userLat, map.userLon);

            if ((prevLocalization.x != currentLocalization.x || prevLocalization.y != currentLocalization.y) && !flag)
            {
                anim.SetBool("walk", true);
                flag = true;
            }
            if ((prevLocalization.x == currentLocalization.x && prevLocalization.y == currentLocalization.y) && flag)
            {
                anim.SetBool("walk", false);
                flag = false;
            }

            yield return new WaitForSeconds(1.2f);
        }
    }
}
