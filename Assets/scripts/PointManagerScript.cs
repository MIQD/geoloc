﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Point
{
    public string pointName;
    public float longtitude;
    public float latitude;

    public string textureName;
    // [NonSerialized]


    public Point()
    {
        pointName = "defaultName";
        textureName = "defaultName";
        longtitude = 0;
        latitude = 0;
    }
}
[Serializable]
public class PointCollection
{
    public Point[] points;
    public PointCollection(int i)
    {
        points = new Point[i];
    }
}

public class PointManagerScript : MonoBehaviour {
    public GameObject player;
    public GameObject markerPrefab;
    public MapNav mapNavScript;
    public PointCollection points;

    private bool loaded = false;

    private float initX, initZ;
    private float height;

    // Use this for initialization
    void Start() {

        mapNavScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<MapNav>();
        height = 3f;
        //12.967040, 77.596617
        //12.967012, 77.596885
        //12.967041, 77.596541
        //12.967118, 77.597210
        //12.966605, 77.596620
    }
    public void Update()
    {
        if(mapNavScript.ready && !loaded)
        {
            loaded = true;                        
            initX = mapNavScript.fixLon* 20037508.34f / 18000;
            initZ = (float)(System.Math.Log(System.Math.Tan((90 + mapNavScript.fixLat) * System.Math.PI / 360)) / (System.Math.PI / 180));
            initZ = initZ * 20037508.34f / 18000;
            TextAsset asset = Resources.Load("points") as TextAsset;
            loadPoints(asset.text, initX, initZ, height);    
        }
    }
    public void loadPoints(string Json, float finitX, float finitZ, float fheight)
    {
        Debug.Log("lodaded");
        points = JsonUtility.FromJson<PointCollection>(Json);
        foreach(Point p in points.points)
        {
            Debug.Log(p.longtitude); 
            Vector3 position = new Vector3(((p.longtitude * 20037508.34f) / 18000) - finitX, fheight / 100, ((Mathf.Log(Mathf.Tan((90 + p.latitude) * Mathf.PI / 360)) / (Mathf.PI / 180)) * 1113.19490777778f) - finitZ);
            GameObject g = (GameObject)GameObject.Instantiate(markerPrefab);
            g.transform.SetParent(this.transform);
            g.name = p.pointName;
            g.transform.position = position;
            Vector3 tmp = transform.eulerAngles;
            g.SetActive(true);
        }
    }
    public void resetPoints()
    {
        for(int i=0; i<this.transform.childCount; i++)
        {
            DestroyObject(this.transform.GetChild(i).gameObject);
        }
        foreach (Point p in points.points)
        {
            Vector3 position = new Vector3(((p.longtitude * 20037508.34f) / 18000) - initX, height / 100, ((Mathf.Log(Mathf.Tan((90 + p.latitude) * Mathf.PI / 360)) / (Mathf.PI / 180)) * 1113.19490777778f) - initZ);
            GameObject g = (GameObject)GameObject.Instantiate(markerPrefab);
            g.transform.SetParent(this.transform);
            g.name = p.pointName;
            g.transform.position = position;
            Vector3 tmp = transform.eulerAngles;
            g.SetActive(true);
        }
    }


}
