﻿using UnityEngine;

public class TouchCamera : MonoBehaviour {

    public Camera cameraComponent;

	Vector2?[] oldTouchPositions = {null,null};
	Vector2 oldTouchVector;
	float oldTouchDistance;
    public float maxZoom = 4f;
    public float minZoom = 15f;
    float distance;

    bool cameraFieldOfVievFlag;

    void Start()
    {
        
    }

	void Update() {

        if(cameraComponent.transform.parent != null && cameraComponent.transform.parent.tag == "Player" && !cameraFieldOfVievFlag)
        {
            cameraFieldOfVievFlag = true;
            cameraComponent.fieldOfView = 100;
        }


        if (Input.touchCount == 0)
        {
            oldTouchPositions[0] = null;
            oldTouchPositions[1] = null;
        }
        else if (Input.touchCount == 1)
        {
            //float x = transform.position.x;
            //if (oldTouchPositions[0] == null || oldTouchPositions[1] != null)
            //{
            //    oldTouchPositions[0] = Input.GetTouch(0).position;
            //    oldTouchPositions[1] = null;
            //}
            //else
            //{
                //Vector2 newTouchPosition = Input.GetTouch(0).position;

                //transform.position -= transform.TransformDirection((Vector3)((oldTouchPositions[0] - newTouchPosition) * cameraComponent.orthographicSize / cameraComponent.pixelHeight * 2f));

                //if (distance > Vector3.Distance(transform.position, transform.parent.position))
                //{
                //    Debug.Log("lll");
                //    transform.position -= transform.TransformDirection((Vector3)((oldTouchPositions[0] - newTouchPosition) * cameraComponent.orthographicSize / cameraComponent.pixelHeight * 2f));
                //}

                //oldTouchPositions[0] = newTouchPosition;

                //transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x, 20, 75), transform.eulerAngles.y, transform.eulerAngles.z);
                //if(transform.eulerAngles.x < 20 )
                //{
                //    transform.eulerAngles = new Vector3(20, transform.eulerAngles.y, transform.eulerAngles.z);
                //}
                //else if( transform.eulerAngles.x > 75)
                //{
                //    transform.eulerAngles = new Vector3(75, transform.eulerAngles.y, transform.eulerAngles.z);
                //}

                //if(transform.localPosition.y > 3 && transform.localPosition.y <9)
                 
                //transform.position = new Vector3(transform.position.x, Mathf.Clamp( transform.position.y, 3, 9), transform.position.z);

                
                //if (transform.position.x < 3)
                //{
                //    transform.position = new Vector3(3, transform.position.y, transform.position.z);
                //}
                //else if (transform.position.x > 9)
                //{
                //    transform.position = new Vector3(9, transform.position.y, transform.position.z);
                //}

                    //Debug.Log(transform.position);
                    //Debug.Log(transform.name);

            //}
        }
        else
        {
            if (oldTouchPositions[1] == null) {
				oldTouchPositions[0] = Input.GetTouch(0).position;
				oldTouchPositions[1] = Input.GetTouch(1).position;
				oldTouchVector = (Vector2)(oldTouchPositions[0] - oldTouchPositions[1]);
				oldTouchDistance = oldTouchVector.magnitude;
			}
			else
            {
				Vector2 screen = new Vector2(cameraComponent.pixelWidth, cameraComponent.pixelHeight);
				
				Vector2[] newTouchPositions = {
					Input.GetTouch(0).position,
					Input.GetTouch(1).position
				};
				Vector2 newTouchVector = newTouchPositions[0] - newTouchPositions[1];
				float newTouchDistance = newTouchVector.magnitude;

                //transform.position += transform.TransformDirection((Vector3)((oldTouchPositions[0] + oldTouchPositions[1] - screen) * cameraComponent.orthographicSize / screen.y));
                 
                cameraComponent.fieldOfView *= oldTouchDistance / newTouchDistance;
                cameraComponent.fieldOfView = Mathf.Clamp(cameraComponent.fieldOfView, maxZoom, minZoom);
                
                //Debug.Log(cameraComponent.transform.localEulerAngles);
                //Debug.Log(cameraComponent.transform.eulerAngles);
                //transform.position -= transform.TransformDirection((newTouchPositions[0] + newTouchPositions[1] - screen) * cameraComponent.orthographicSize / screen.y);

                oldTouchPositions[0] = newTouchPositions[0];
				oldTouchPositions[1] = newTouchPositions[1];
				oldTouchVector = newTouchVector;
				oldTouchDistance = newTouchDistance;
			}
		}

        
    }
}
