﻿using UnityEngine;
using System.Collections;

public class TouchInputController : MonoBehaviour {

    public static bool freewalk = true; //true, if there is no panel open

    public LayerMask interactableMask;
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0) && freewalk)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool hitInteractable = Physics.Raycast(ray, out hit, 100f, interactableMask);

            if (hitInteractable)
            {
                if (hit.transform.GetComponent<IInteractable>() != null)
            {
                    IInteractable reference = hit.transform.GetComponent<IInteractable>();
                    reference.OnClickMethod();
            }

            }
        }
	}
}
